This repo is a result of the Introduction to Vue tutorial from the Vue Mastery website.

By completing this tutorial, you will understand the basics of Vue and have a good idea how it stacks up against other frameworks such as React and Angular.

The final product is a basic online store which has a product component and is able to records reviews against the product (see screenshot below).

https://gitlab.com/brandonlabs/vue-tutorial/uploads/db7b80dc1b4fe0d2315e48417f2fb166/Screen_Shot_2018-08-24_at_18.30.18.png